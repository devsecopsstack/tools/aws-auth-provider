import base64
import logging
import os
import re
import tempfile
from functools import cache
from http import HTTPStatus
from typing import Optional

import boto3
from fastapi import FastAPI, HTTPException, Query, Request, Response
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse
from starlette.exceptions import HTTPException as StarletteHTTPException

logger = logging.getLogger("aws-auth-provider")
app = FastAPI()


# manages the different AWS authentication methods
def configure_boto(env_ctx: str = None, region: str = None, role_arn: str = None):
    # auto-determine env type
    if not env_ctx:
        env_ctx = guess_env_ctx()

    # set region
    if region is None:
        region = (
            getenv_cleared(f"AWS_{env_ctx}_REGION")
            or getenv_cleared("AWS_REGION")
            or getenv_cleared("AWS_DEFAULT_REGION")
        )
    if not region:
        logger.error("AWS region not found")
        raise HTTPException(status_code=400, detail="AWS region not found")
    os.environ["AWS_DEFAULT_REGION"] = region

    # determine auth method
    jwt_token = os.environ.get("AWS_JWT")
    if role_arn is None:
        role_arn = getenv_cleared(f"AWS_{env_ctx}_OIDC_ROLE_ARN") or getenv_cleared(
            "AWS_OIDC_ROLE_ARN"
        )
    if jwt_token and role_arn:
        # Assume Role with Web Identity Provider
        # see: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html#assume-role-with-web-identity-provider
        logger.info("Auth method: STS Assume Role with Web Identity Provider")
        with tempfile.NamedTemporaryFile(
            mode="w", encoding="utf-8", delete=False
        ) as token_file:
            token_file.write(jwt_token)
            token_file.close()
        os.environ["AWS_ROLE_ARN"] = role_arn
        os.environ["AWS_WEB_IDENTITY_TOKEN_FILE"] = token_file.name
        os.environ[
            "AWS_ROLE_SESSION_NAME"
        ] = f"GitLabRunner-{os.getenv('CI_PROJECT_ID')}-{os.getenv('CI_PIPELINE_ID')}"
        return

    access_key_id = getenv_cleared(f"AWS_{env_ctx}_ACCESS_KEY_ID") or getenv_cleared(
        "AWS_DEFAULT_ACCESS_KEY_ID"
    )
    secret_access_key = getenv_cleared(
        f"AWS_{env_ctx}_SECRET_ACCESS_KEY"
    ) or getenv_cleared("AWS_DEFAULT_SECRET_ACCESS_KEY")
    if access_key_id and secret_access_key:
        # see: https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html#environment-variables
        logger.info("Auth method: basic (access key ID & secret access key)")
        os.environ["AWS_ACCESS_KEY_ID"] = access_key_id
        os.environ["AWS_SECRET_ACCESS_KEY"] = secret_access_key
        return

    logger.error("Auth method not found: no credentials available")
    raise HTTPException(status_code=401, detail="AWS credentials not found")


def guess_env_ctx() -> str:
    # guess from GitLab CI predefined vars
    ref_name = os.getenv("CI_COMMIT_REF_NAME", "-")
    prod_ref = os.getenv("PROD_REF", "/^(master|main)$/").strip("/")
    if re.match(prod_ref, ref_name):
        # could be staging or prod
        if os.getenv("CI_JOB_STAGE", "-") in [
            "publish",
            "infra-prod",
            "production",
            ".post",
        ]:
            return "PROD"
        return "STAGING"

    integ_ref = os.getenv("INTEG_REF", "/^develop$/").strip("/")
    if re.match(integ_ref, ref_name):
        return "INTEG"

    return "REVIEW"


# Workaround the GitLab bug with forced exposed variables:
# variables:
#   SOMEVAR: "$SOMEVAR"
# os.getenv("SOMEVAR") may have value '$SOMEVAR' if the variable is not defined as a project variable
def getenv_cleared(name: str) -> Optional[str]:
    value = os.getenv(name)
    return None if value == f"${name}" else value


@app.get("/health", response_class=PlainTextResponse)
def ping():
    return "ok"


@app.exception_handler(Exception)
async def uncaught_exception_handler(_: Request, exc: Exception) -> Response:
    return PlainTextResponse(
        content=f"Internal Server Error\nCause: [{exc.__class__.__qualname__}] {str(exc)}",
        status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
    )


@app.exception_handler(StarletteHTTPException)
async def http_exception_handler(_: Request, exc: StarletteHTTPException) -> Response:
    headers = getattr(exc, "headers", None)
    return PlainTextResponse(
        content=exc.detail, status_code=exc.status_code, headers=headers
    )


@app.exception_handler(RequestValidationError)
async def request_validation_exception_handler(
    _: Request, exc: RequestValidationError
) -> Response:
    return PlainTextResponse(
        status_code=HTTPStatus.UNPROCESSABLE_ENTITY,
        content=jsonable_encoder(exc.errors()),
    )


@app.get("/ecr/auth/token", response_class=PlainTextResponse)
@cache
def get_ecr_auth_token(
    env_ctx: str = Query(default=None, alias="env_ctx"),
    region: str = Query(default=None, alias="region"),
    role_arn: str = Query(default=None, alias="role_arn"),
) -> str:
    configure_boto(env_ctx, region, role_arn)
    # see: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecr/client/get_authorization_token.html
    client = boto3.client("ecr")
    response = client.get_authorization_token()
    return response["authorizationData"][0]["authorizationToken"]


@app.get("/ecr/auth/username", response_class=PlainTextResponse)
def get_ecr_auth_username(
    env_ctx: str = Query(default=None, alias="env_ctx"),
    region: str = Query(default=None, alias="region"),
    role_arn: str = Query(default=None, alias="role_arn"),
) -> str:
    b64token = get_ecr_auth_token(env_ctx=env_ctx, region=region, role_arn=role_arn)
    # token is base64("<username>:<password>")
    user_password = base64.b64decode(b64token).decode("utf-8")
    return user_password.split(":")[0]


@app.get("/ecr/auth/password", response_class=PlainTextResponse)
def get_ecr_auth_password(
    env_ctx: str = Query(default=None, alias="env_ctx"),
    region: str = Query(default=None, alias="region"),
    role_arn: str = Query(default=None, alias="role_arn"),
) -> str:
    b64token = get_ecr_auth_token(env_ctx=env_ctx, region=region, role_arn=role_arn)
    # token is base64("<username>:<password>")
    user_password = base64.b64decode(b64token).decode("utf-8")
    return user_password.split(":")[1]